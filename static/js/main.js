$(document).ready(function() {
    
    $("#change_theme_button").click(function() {
        var body_color = $('body').css('color');
        if (body_color == "rgb(255, 255, 255)") {
            $("body").css("color", "black");
            $("body").css("background-color", "#fcfcfc");
            $(".accordion-header").css("background-color", "#80a1b0");
            $(".accordion").css("color", "white");
            $(".accordion").css("background-color", "#7095a6");
            $("#gmail-logo").attr("src", gmail_black);
            $("#twitter-logo").attr("src", twitter_black);
            $("#github-logo").attr("src", github_black);
            $("#change_theme_button").css("background-color", "#80a1b0");
            $("#change_theme_button").css("color", "white");

        } else {
            $("body").css("color", "white");
            $("body").css("background-color", "#383c4a");
            $(".accordion-header").css("background-color", "white");
            $(".accordion").css("color", "black");
            $(".accordion").css("background-color", "#f2f2f2");
            $("#gmail-logo").attr("src", gmail_white);
            $("#twitter-logo").attr("src", twitter_white);
            $("#github-logo").attr("src", github_white);
            $("#change_theme_button").css("background-color", "white");
            $("#change_theme_button").css("color", "black");
        }    
    })

    $('.accordion-header').click( function() {
        $(this).next().slideToggle();
    })

    $('.accordion-header').mouseenter(function() {
        var header_color = $(this).css('color')
        if (header_color == "rgb(255, 255, 255)") {
            $(this).css("background-color", "#6ca0c5");
        } else {
            $(this).css("background-color", "#e5e5e5");   
        }

    }).mouseleave(function() {
        var header_color = $(this).css('color')
        if (header_color == "rgb(255, 255, 255)") {
            $(this).css("background-color", "#80a1b0");
        } else {
            $(this).css("background-color", "white");   
        }
    })

    var pos = 0;
    $(window).scroll(function () {
        var scroll = $(this).scrollTop();
        if (scroll > pos) {
            $("#change_theme_button").fadeIn();
        } else {
            $("#change_theme_button").fadeOut();
        }
        pos = scroll;

    })
    
});

$('#change_theme_button').mouseenter(function() {
    var button_color = $(this).css('color')
    if (button_color == "rgb(255, 255, 255)") { //putih
        $(this).css("background-color", "#6ca0c5");
    } else {
        $(this).css("background-color", "#e5e5e5");
    }

}).mouseleave(function() {
    var button_color = $(this).css('color')
    if (button_color == "rgb(255, 255, 255)") {
        $(this).css("background-color", "#80a1b0");
    } else {
        $(this).css("background-color", "white");   
    }
})