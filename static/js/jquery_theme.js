$(document).ready(function(){

    var count_bg = 0;
    var count_col = 0;
    var bg = ["#caf6f3", "white", "#8db0c9"]
    var color = ["white", "#caf6f3"];

    var i = 0;
    document.querySelector("button").addEventListener("click", function(){
        i = i < bg.length ? ++i : 0;
    document.querySelector("body").style.background = bg[i];
    document.querySelector("h1").style.color = color[i];
    });
});